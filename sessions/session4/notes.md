chemin substitutionel est une imputation des données manquantes, à partir des données sous un modèle particulier.
On peut utiliser ça pour inférer des paramètres du modèles.
On peut résumer l’ensemble du chemin à travers des statistiquse suffisantes: le nombre total de substitution entre chaque paires de codon et le temps de résidence dans chacun des codons.
On peut les collecter globalement, par branche, par site, etc ...

Premier niveau de compaction.

On veut moduler de dN/dS à travers les branches.
On aura donc un vecteur de comptage de chacun des codons, et une matrice de temps de résidence.

Si on s’intéresse aux modulations des valeurs de dN et dS le long d’une branche, il faut prendre en compte le nombre de subst. n & s observées, et le nombre efficace de cibles.

Le nombre effectif de cibles dépend de beaucoup du taux de mutations nucléotidiques.

Ls = combien de mutations syn j’attends par unité de longueur de branche. Ls est pondéré par l’histoire des changements le long de la branche.

Ce sont des statistiques suffisantes.

Les statistiques suffisantes données par le mapping peuvent être utilisées dans un contexte bayésien ou en ML.

Le mapping permet aussi de collecter des statistiques suffisantes concernant le patron de substitution nucléotidique.

Le mapping de substitution est une reconstruction de l’histoire d’une branche.
On peut utiliser le mapping pour estimer le temps passé dans un état donéne.
Cela peut donner une idée de la stabilité évolutive de certains acides aminés.
C’est complémentaires à la reconstruction de séquences ancestrales.

La première utilisation faites par Nielsen, via simulations.
Seconde implémentation analytique par Minin & Suchard 2008.

On veut calculer l’espérance d’avoir, selon un modèle, en moyenne, on a un nombre d’événement donné.

Mapping conditionnel: on sait ce qu’il y a au début et à la fin de chaque branche.
Mais les séquences ancestrales sont inconnues.
Alors si on intègre sur tous les états ancestraux possibles, on peut calculer l’espérance sur tous les scénarios possible qui donneraient l’état ancestral aux nœuds.
Le premier niveau: étant donné le modèle M, on peut calculer en moyenne ce qu’il s’est passé.
Second niveau: on calcule en moyenne ce qu’il s’est passé pour tous les sites.
Espérance du nombre d’événement.
Ces calculs sont très rapides.

On peut l’utiliser pour estimer des paramètres d’un modèle; par exemple \kappa le taux de ts/tv, peut être estimé en comptant le nombre de ts et le nombre de tv.

On ne calcule pas des taux, on compte des événements. Ce sont des comptes par sites “pertinents”.
Le but est de revenir à des taux pour estimer nos paramètres.

Plutôt que c’est le nombre de sites non-synonyme, on peut dire que c’est ce que ferait un modèle neutre sur les mêmes données.
Les dS & dN dépendent du modèle nucléotidique sous-jacent (taux de GC par exemple).

La permissivité du modèle est le nombre attendu de substitutions qu’un modèle retourne sur une branche, connaissant les séquences ancestrales.
dS sera alors le nombre de subst. synonymes observées par rapport à ce que le modèle neutre “aurait” pu faire sur les mêmes données.

L’approche est la suivante:
