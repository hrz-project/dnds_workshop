---
title: "Introduction to codon models"
author: >
  Thibault Latrille,
  Nicolas Lartillot,
  Laurent Guéguen
date: "2021-03-10 14:00"
output: tint::tintPdf
bibliography: references.bib
latexfonts:
  - package: AlegreyaSans
    options: sfdefault
  - package: FiraMono
    options: scaled=0.7
---

# Introduction to codon models

Some mutations change the amino-acid.
L’histoire des substitutions along the species tree conduit à un alignement de séquences codantes.

En contrastant les deux régimes qui affectent les deux types de positions.

Le paramètre \omega est un facteur d’échelle qui affecte les mutations ns par rapport aux mutations s.

L’idée c’est de trouver des lignées, des sites, des gènes qui sont affectés par de la sélection positive ou négative (diversifiante ou purificatrice).

Question: pourquoi 1 est un bon seuil entre régimes de sélection?
Quel est l’attendu réellement neutre?
On peut avoir un omega de 1 avec un mélange de régimes le long de la proteine?

Quel est l’attendu sous sélection purifiante?

Comment gérer le fait que l’on sache que les mutations s sont parfois sous sélection?

Sites cachés de la protéines sont généralement plus contraints que ceux à l’extérieur de l’enveloppe de la prot.

- Qu’est-ce que omega represente?
- comment tester les modèles à codons?
- est-ce que ces modèles nous donne une estimée valide de omega?

Substitution = mutation à fixation.

La probabilité de fixation d’un nouvel allèle dépend uniquement (quand s est petit) du coefficient de sélection et de Ne.

On peut dessiner trois régimes de sélection: 2NePfix ou Pfix est la probabilité de fixation de l’ordre de 1, inférieur ou supérieur à 1.

Nicolas préfère parler de dN/dN0, et utiliser dS comme proxy de dN0: quel serait le patron de fixation en l’absence de toute sélection sur les acides-aminés?

Capturer l’effet net de la sélection?

Modèle à codon (61x61) excluant les codons stops.

Qij = µij synonymous
Qij = µij non-syn

L’espérance étant que omega représente l’effet net de la sélection au niveau protéique. En espérant que omega = dN/dS.

Il faut distinguer l’effet de la sélection (la probabilité moyenne de fixation des non-synonymes) du modèle à codon.

Le but est de construire des modèles à codons qui séparent d’un côté le taux de mutation et de l’autre l’effet net de la sélection.

Le modèle sous-jacent de substitution est relativement simple et ne fait généralement pas d’hypothèse sur le patron de mutation multi-sites (di-Nucleotide process?)

Modèle HKY introduit un paramètre \kappa qui capte le taux relatif de transitions / transversions (kappa = 2 => ti sont 2x plus fréquentes que les tv).

Le modèle HKY est généralement le modèle sous-jacent aux modèles à codon. On peut construire des matrices plus complexes mais celle-ci est généralement celle de départ.

Les différents paramètres sont fités conjointement aux données.
On peut les fixer empiriquement si on les connaît bien, mais ça n’est pas très fréquent.

Les substitutions synonymes ont lieu au même taux à toutes les positions (1 ou 3 des codons). Les processus mutationels sont a priori aveugles à la structure en codon et se comportent de la même façon sur toutes les positions des codons.

Si l’on pose w = 1, alors on a un simple processus mutationnel (en dehors des codons stop).

La paramétrization de Muse & Gaut se justifie bien en termes de processus de mutation aveugle aux codons + sélection au niveau des acides-aminés.

Sylvain remarque qu’on aurait pu laisser les codons stops.

Le modèle de Goldman & Yang a deux versions (F61 et F1x4).
Au lieu d’un vecteur de fréquences ACGT, on a un vecteur de fréquences sur l’ensemble des 61 codons.
La transition de ACA vers ACC dépend de la fréquence du *codon*, pas des bases sous-jacentes.
On perd l’interprétation en termes de taux de mutation.

La différence essentielle entre Muse et Gaut et GY est que tout dépend de la fréquence du *codon* final.

Problème avec MG & GY: prédisent tout deux les mêmes fréquences nucléotidiques aux trois positions des codons. Or on sait très bien que ça n’est pas le cas. Le modèle GY F3x4 est plus fit et introduit un paramètre pi qui varie selon les positions 1, 2 et 3.

Nicolas maintient que sur le plan logique, le modèle GY est moins fondé que celui de Muse & Gaut.

Il semblerait que la paramétrisation de Muse & Gaut présentent les meilleurs justifications mécanistiques.
Mais même elle ne capture pas tout à fait les aspects sélection/mutation.
