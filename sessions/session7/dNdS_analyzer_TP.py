#!/usr/bin/python
import re
from io import StringIO
import sys
from ete3 import EvolTree
import pandas as pd
import numpy as np
import sys
import argparse
import os
import subprocess

# Print out a message when the program is initiated.
print('------------------------------------------------------------------------------------------------------------------------------\n')
print('#dNdS_analyzer.\n')
print('\n')
print('This script will create bash scripts in order to run dNdS analysis for cluster of genes.\n')
print('Each analysis will fill a dataframe with \n')
print('\n')
print(' Cluster_name  Mean_dNdS   Pvalue_dNdS\n')
print(' cluster1      0.34        0.20           <------- foreground branches are not significantly different from 1.    \n')
print(' cluster2      0.002       0.000003       <------- foreground branches are significantly different from 1.\n')
print('\n')
print('foreground branches being the hymenoptera candidate loci for a viral domestication. \n')
print('If foreground branches are significantly different from 1, then the dN/dS values are under purifying or adaptative selection. \n')
print('------------------------------------------------------------------------------------------------------------------------------\n')
#----------------------------------- PARSE SOME ARGUMENTS ----------------------------------------
parser = argparse.ArgumentParser(description='Allow to make busco jobs for slurm')
parser.add_argument("-tree", "--tree",help="Gene phylogeny of the cluster in nwk format")
parser.add_argument("-aln", "--alignment",help="Codon alignment of the cluster (in fasta format)")
parser.add_argument("-o", "--output_file", help="The directory where the output will be written")
parser.add_argument("-c", "--cluster_name", help="The cluster name")
parser.add_argument("-l", "--list_species",nargs='+',help="List of species to test")
parser.add_argument("-paml", "--paml_path",help ="The full path to paml")


args = parser.parse_args()

# Variable that stores fasta sequences
Aln=args.alignment
Tree=args.tree
Output_path=args.output_file
list_of_species_to_test=args.list_species
Paml=args.paml_path
Cluster_name=args.cluster_name

from time import process_time
t1_start = process_time()

print("list of loci to test;",list_of_species_to_test)


print("----------------------------------------------------------------")
print("Beginning of the dNdS analysis for the cluster :"+Cluster_name)
print("----------------------------------------------------------------")
print("\n")


#Open the tree and the alignment codon file of the cluster of interest
print("- The tree and the alignment codon file of the cluster of interest have been charged -")
tree = EvolTree(Tree,binpath=Paml)
R = tree.get_midpoint_outgroup()
tree.set_outgroup(R)
new_tree=tree.write (format=0)
tree = EvolTree(new_tree,binpath=Paml)#Link to your paml soft 
print(tree)
print("\n")
print('Tree of the cluster : ', Cluster_name)

#Get nodeids
df_marks = pd.DataFrame(columns=['Node_name', 'Node id'])
ancestor=tree.get_common_ancestor(list_of_species_to_test)
print("Foreground branches:")
print(ancestor)
print("Type4")
print(type(ancestor))
for node in ancestor.traverse("levelorder"):
	try:
		df_marks=df_marks.append({"Node_name":node.name,"Node_id":node.node_id}, ignore_index=True)
	except:
		continue


#In order to deal with small phylogenies
if df_marks.shape[0] < 5:
	df_marks=df_marks.loc[df_marks["Node_name"].str.contains("__")]
else:
	df_marks = df_marks.iloc[1:]

marks=list(df_marks['Node_id'].astype(int))
tree.workdir = Output_path
#Link the tree to the alignment file 
tree.link_to_alignment(Aln)

# mark a group of branches of interest

print("\n")
print("-------------------------------------------------------------------------")
print(".  Adding of node marks...                                               ")
print(".  the marks will allow to force the loci to be set to a dN/dS value =1  ")
print("-------------------------------------------------------------------------")


tree.mark_tree(marks, ['#1'])
print('node marked :')
print(tree.write ())
print("\n")

#Running the models 
print("--------------------------------------------------")
print("      Running of the the neutral model         ")
print(" all marked sequence are forced to have a dNdS =1 ")
print("--------------------------------------------------")


tree.run_model('b_neut'+'.'+str(Cluster_name),CodonFreq=4,estFreq=1,getSE=1,noisy=3,verbose=1)
print("neutral model analysis done.")
print("\n")
print("--------------------------------------------------")
print("      Running of the the free-ratio model            ")
print(" all marked sequence are allowed to be free       ")
print("--------------------------------------------------")
tree.run_model('b_free'+'.'+str(Cluster_name),CodonFreq=4,estFreq=1,getSE=1,noisy=3,verbose=1)
print("free-ratio model analysis done.")
print("\n")
tree.link_to_evol_model(Output_path+'b_neut'+'.'+str(Cluster_name)+'/out', 'b_neut')
for model in  tree._models:
		fb_model_neut=tree.get_evol_model(model)
tree.link_to_evol_model(Output_path+'b_free'+'.'+str(Cluster_name)+'/out', 'b_free')
for model in  tree._models:
		fb_model_free=tree.get_evol_model(model)
#Charging the free model output
df_free = pd.read_csv(StringIO(fb_model_free.__str__()), skiprows=6,names=['marks','omega','node_ids','name'])
df_free = df_free.applymap(lambda x: x.split(":")[1])
#Remove white space
df_free=df_free.applymap(str.strip).rename(columns=str.strip)
print("\n")
print(df_free)
omega=[]
for i in list_of_species_to_test:
		s = df_free.loc[df_free['name'] == i,'node_ids']
		dNdS = df_free.loc[df_free['name'] == i,'omega']
		dNdS = float(dNdS)
		s=int(s)
		omega.append(dNdS)
print("Free model output: ")
print(df_free)
print("\n")
print("dN/dS average is :",omega[0]) #Take the first omega value
print("\n")
#Run of the likelihhod ratio test...
print("------------------------------------------------------------------")
print("           Running of the likelihood ratio test                   ")
print("                                                                  ")
print("         if pvalue 'b_free' vs 'b_neut' < 0.05:                   ")
print(" the foreground branches are significantly different from 1 '     ")
print("------------------------------------------------------------------")
try:
    		from scipy.stats import chi2
    		chi_high = lambda x, y: 1 - chi2.cdf(x, y)
except ImportError:
    		from .utils import chi_high

def get_most_likely2(altn, null):
 		if hasattr(altn, 'lnL') and hasattr(null, 'lnL'):
  			if  null.lnL - altn.lnL < 0:
    				return chi_high(2 * abs(altn.lnL - null.lnL),float(altn.np - null.np))
  			else:
  				warn("\nWARNING: Likelihood of the alternative model is ""smaller than null's (%f - %f = %f)" % (null.lnL, altn.lnL, null.lnL - altn.lnL) + "\nLarge differences (> 0.1) may indicate mistaken ""assigantion of null and alternative models")
  				return 1
pvalue=get_most_likely2(fb_model_free,fb_model_neut)
print("pvalue estimated :", pvalue)
print("\n")
#get the Standar Error of the dN/dS estimation :
with open(Output_path+'b_free'+'.'+str(Cluster_name)+'/out', "r") as ifile:
		for line in ifile:
			if line.startswith("SEs for parameters:"):
				SE=next(ifile, ' ').strip()
				SE=re.split('\s+', SE)
				SE=SE[-1]
#Insert the value into a new dataframe
df = pd.DataFrame(columns=("Clustername","Mean_dNdS","Pvalue_dNdS"))
df=df.append({"Clustername":Cluster_name,"Mean_dNdS":omega[0],"Pvalue_dNdS":pvalue,"SE_dNdS":SE},ignore_index=True)
print("Global result :")
print(df)
df.to_csv(Output_path+"dNds_"+Cluster_name+".out",sep="\t",index=False)

t1_stop = process_time()
print("Elapsed time:", (t1_stop-t1_start)/60)
