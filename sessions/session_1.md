---
title: "dN/dS User Feedback "
author: "s. barreto"
date: "2021-02-10 16:31"
output: tint::tintPdf
bibliography: references.bib
latexfonts:
  - package: AlegreyaSans
    options: sfdefault
  - package: FiraMono
    options: scaled=0.7
---

- **abbreviations**:
  - $\omega$ = dN/dS
  - $N_{\text{e}}$ = effective population size

# Talk Tristan Lefébure: Usage of dN/dS in Evol Mol:

## $\omega$ as $N_{\text{e}}$ proxy

The $\omega$ ratio is often used as proxy of Ne. In classical
population genetics models, $\omega$ depends on $N_{\text{e}}$ and selection
coefficient $s$. So with stable $s$, $\omega$ becomes a good proxy of $N_{\text{e}}$.

@LefebureLessEffectiveSelection2017 compared $N_{\text{e}}$ between pairs of
isopods species, one of which transitionned to an underground
lifestyle, the other not, in many pairs all over the isopods tree. The
reduction of resources associated to a underground lifestyle
transition was expected to lead to a reduction in $N_{\text{e}}$.
They used (low-coverage?) transcriptomic data.

Under the assumption that $s$ did not vary significantly across all
genes when transitionning to underground, a higher dN/dS is suggesting
higher $N_{\text{e}}$. Data support this hypothesis.

A free-ratio mode was used: $\omega$ is allowed to vary between
branches along the tree. A problem is that if some genes are under
positive selection because of selective effect linked to the lifestyle
transition, we would observe a global increase in dN/dS due to
selection on some non-synonymous changes.

To counter this argument, they compared dN/dS using branch-site
models: the $\omega$ ratio is allowed to vary between branches *and*
between sites. Most sites are under purifying selection; and if you
focus only on those sites that are under purifying selection, you
still get the signal of higher dN/dS in underground species compared
to their sister surface species.^[Model used was the M10 model,
implemented in bppML.] This confirms that $N_{\text{e}}$ plays the most part in
these dN/dS shifts, not selective effects.

## dS as a mutation rate proxy

Under the assumption that synonymous substitutions *are* strictly
neutral, then $dS \approx \mu$. They used this property to compare
mutation rate between surface and underground species, using the same
pairwise/sister species comparison design. dS is indeed lower in
underground species (probably because growth rate is lower in these
species), but strangely *not* in mitochondrial genomes.

However the longer the branch, the higher the dS; and so dS, if
comparable between sister species, don’t give rise to meaningful
comparison accross the whole tree, unless you normalise it somehow. A
way to proceed would be to normalise by divergence *time*, but this
requires a chronogram, an ultrametric tree.

Another way is to use coevol, which jointly do both, if furnished some
calibration dates; they used this to get a posterior estimation of dS
along branches, by arbitrary unit of time, setting the root nodes as 1
arbitrary.^[FIXME, not clear to me] The rationale is that, in the
absence of calibration dates, one could still calibrate the tree by
setting the oldest divergence date to the root to 1 and coevol will
produce a dS by time unit.

dS seemed to reach saturation in mitochondrial genomes, with about
4-fold higher dS in mitochondria than nuclear genomes. And so
mitochondria seem to evolve faster than the nuclear genome, in a
disjoint manner.

One way to reduce the signal saturation is to focus on synonymous
transversions; transversions are less frequent and thus less
saturated. You do lose signal though.

Nicolas noted that saturation of dS is important because it has the
potential to artificially distort the dN/dS; dN will plateau slower
than dS in case of saturation, and so dN/dS will be overestimated.

Tristan mentioned mapNH-based simulations where mapNH retrieved the
substitution model quite effectively even with clear saturation
effects. Laurent D. noted that mapNH assumes a stable $\omega$.

> Care should be taken when interpreting $\omega$ if dS is saturated.

This experimental design also allows studying the influence of
environmental factors on dS (mutation rate). In isopods, radioactivity
increases the mitochondrial mutation rate.

Tristan mentionned that mitochondrial genomes must evolve in a
somewhat clocklike manner for mitochondrial-genes based species
delimitation to work that well.

## $\omega$ as pseudogene scan

Underground species lose their eyes; this natural selection relaxation
leads to pseudogeneization of opsins. Opsins are among the most
well-conserved protein across metazoans.

Parameter-rich models (à la codeml) are not very efficient on a single
ortholog family because they are to be fit across many sites.
Mapping-based approaches like mapNH and testNH were used to gather
branches with similar substitution models, to reduce the number of
potential models. These models clearly showed a higher $\omega$ value
for opsins of underground species, consistent with selective pressure
relaxation.

Another hypothesis tested was to see if opsins accumulated more
pseudogeneization signals in underground species. Underground species
accumulated more stop codons, at a speed suggesting gene-loss could be
adaptive. They (with Laurent Guéguen) developped models to get an
estimation of the probability to switch to a stop codon.

Laurent mentioned that in Paramecia, stop codons accumulated more
readily in no-longer expressed genes, suggesting that a stop of
expression precedes the accumulation of stop codon. This suggest that
it is deleterious to express pseudogenes, or truncated genes.

Consistent with that, Tristan did not see a 5’ accumulation of stop
codons, suggesting that these genes were already unexpressed.

## $\omega$ as pseudogeneisation timing estimation

$\omega$ can also be used to scan for pseudogeneized genes; and under
a simple model it can be used to get a relative estimate of the
proportion of a diverging branch corresponding to the time passed in
the underground state: say 20% of the branch leading to this species
was in the underground.

## confounding factor of $\omega$

Bad sequences can clearly blur $\omega$: badly assembled genomes lead
to frameshift, artificially increasing dN; gene or domain transfer can
blur the strict orthology signal. Unreliable species tree do too.


# Florian / Laurent / Anouk

Case study of using $\omega$ as $N_{\text{e}}$ proxy, as the metazoan scale.

Florian Used a set of 978 BUSCOv3 genes (USCOs here), to scan for
1-to-1 orthologs across 53 species of Metazoa. At this scale, hidden
paralogy can hurts, and so Florian parsed the BUSCO output to get
strictly orthologous genes and remove ambiguous annotations.

It gives a distribution of number of species with a given USCO; 45
species were kept with a total of 917 USCOs. Alignment were done with
prank, with no guide-tree. Prank has this interesting property of not
trying to align unalignable sites.

The total concatenated alignment gives 1292781 sites.
RAXmlNG was used to infer the species tree.

To get an estimate of dN/dS, in a parallelizable way, Florian then
splitted the concatenated alignment in 19 subsets, and ran BppML on
each subset, then verified dN/dS estimates were coherent across
subsets. It is, but can vary slightly between subsets.

> Comparing dN/dS across species should rely on the same set of
> orthologs.

MapNH was used to estimate dN/dS. The way it works is to correct an
homogeneous model used as starting point by adjusting it to the data
in a branch-wise manner. The mean value will change from one subset to
the other, which explain why subset can be slightly uncorrelated.

Bastien suggested to use IQtree to obtain an average ML estimate of
dN/dS, and subsequently to use mapNH to correct this estimate by the
subset-data. IQTree is more readily parallelizable than Bio++, and was
developped with performance in mind (use vectorization CPU instructions).

Another way to paliate this would be to resort to raw count instead of
dS rate, and “manually” get the rate by normalising observed count of
synonymous substitution with number of sites susceptible to give rise
to a synonymous substitution (observed/susceptible), taking into
account transition/transversion bias, AT-bias, etc.

Florian observed a clear correlation between $\omega$ and species
longevity, as observed in other species, but not in birds (see
Laurent’s point below).

Nicolas mentionned that in species where lots of polymorphism
segregate, this could contribute to an overestimation of dN, because
$\pi N/\pi S$ tends to be higher than dN/dS. One would to check this
would be to compare dN/dS along the terminal branches with the
estimate at immediate higher-level branches. Most terminal branches
are quite long here in this metazoan dataset.

Nathanaëlle Saclier with Tristan did something akin to this by
checking if $\omega$ estimates were affected by removing polymorphic
sites (intraspecific diversity).

Something to keep in mind is that the USCO dataset was chosen to
compare the same genes between really distant species. Orthologs are
thus slowly evolving families.

# Laurent: gBGC as confounding factor in dN/dS estimations

Laurent described the intriguing results of
@BolivarGCbiasedGeneConversion2019. The context is that in birds, the
body mass (or longevity) / $\omega$ positive correlation does not
hold, which could suggest that $N_{\text{e}}$ and adult body mass are not
correlated across avian species. The authors tested if this could be
explained by GC-biased gene conversion, a fixation bias for G and C
bases linked to an intrinsic bias in the recombination intermediate
resolution machinery. The intensity of gBGC depends on $N_{\text{e}}$, as does
$s$. What @BolivarGCbiasedGeneConversion2019 observed is that when you
look only at GC-conservatives substitutions, *ie* sites a priori
immune to gBGC, the dN/dS measured is indeed correlated with body
mass.^[They used mapNH on different categories of sites (SW, WW and
SS)]

> To use dN/dS as an unbiased estimate of $N_{\text{e}}$, one need to take gBGC
> into account. As Churchill would put it, where there is great $N_{\text{e}}$
> there might be great gBGC.

One way to paliate this is to target only GC-conservatives sites, at
the cost of losing power.

# Benjamin: looking for shifts in selection regimes

Benjamin Guinet uses dN/dS to look for domestication of viral
sequences following endogeneisation by their host, across Hymenoptera.

One way domestication manifests itself in the molecular record is by
purifying selection following endogeneisation, under the hypothesis
that ancestral viral function is beneficial to the host as it is.

Benjamin developed a pipeline to look for monophyletic groups of
endogeneised sequences in a background tree of viral sequences. He
then compares the likelihood of two models via the ETE3 interface to
codeML. The two models are a free-ratio model ($\omega$ varies across
all branches) and an homogeneous model (constant $\omega$ across
branches). A statistical procedure was used to remove spurious signals.

# Sylvain: how do pairwise *vs* tree-wise measure of dN/dS compare?

The pairwise estimate of dN/dS is the most parcimonious number of
non-synonymous and synonymous changes that explain the differences
between two sequences. It is unbiased as long as there is no
saturation. This a model-free estimate, as Tristan put it. It doesn’t
assume any substitution model, because it can’t.

Benjamin linked to @AngelisBayesianEstimationNonsynonymous2014, a
paper by the Yang team. They implemented a Bayesian estimate of
$\omega$ by placing a prior on $\omega$ that prevents it from reaching
$0$ or $\infty$. Uninformative sequences with only N or only S
substitutions would give either 0 or $\infty$ as a ratio. The prior
dominate the likelihood in those case.

# Conclusion



# References
